package com.example.courier_system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
public class CourierSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(CourierSystemApplication.class, args);
    }

}
