package com.example.courier_system.service;

import com.example.courier_system.dto.OrderDTO;

public interface PlaceOrderService {

    boolean placeOrder(OrderDTO orderDTO) throws Exception;

    boolean updatePlaceOrder(OrderDTO orderDTO) throws Exception;

    boolean deletePlaceOrder(Long orderId) throws Exception;

    OrderDTO getOrders(Long userId);

}
