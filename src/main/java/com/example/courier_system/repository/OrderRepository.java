package com.example.courier_system.repository;

import com.example.courier_system.dto.OrderDTO;
import com.example.courier_system.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query("SELECT new com.example.courier_system.dto.OrderDTO(" +
            " od.id,\n" +
            " od.senderName,\n" +
            " od.senderAddress,\n" +
            " od.receiverName,\n" +
            " od.receiverAddress,\n" +
            " od.parcelWeight,\n" +
            " od.parcelType,\n" +
            " c.name)" +
            " FROM Order od INNER JOIN Courier c ON od.courierCompanyCode = c.code\n" +
            " WHERE od.userId=:userId")
    OrderDTO getOrderByUserId(@Param("userId") Long user);

}
