package com.example.courier_system.repository;

import com.example.courier_system.entity.Courier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourierRepository extends JpaRepository<Courier, Long> {

    Courier findFirstByCode(Long code);

}
