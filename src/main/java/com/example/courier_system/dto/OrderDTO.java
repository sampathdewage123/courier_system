package com.example.courier_system.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {

    private Long id;
    private String senderName;
    private String senderAddress;
    private String receiverName;
    private String receiverAddress;
    private String parcelWeight;
    private Long courierCompanyCode;
    private String parcelType;
    private String courierCompanyName;
    private Long userId;

    public OrderDTO(Long id, String senderName, String senderAddress, String receiverName, String receiverAddress,
                    String parcelWeight, String parcelType, String courierCompanyName) {
        this.id = id;
        this.senderName = senderName;
        this.senderAddress = senderAddress;
        this.receiverName = receiverName;
        this.receiverAddress = receiverAddress;
        this.parcelWeight = parcelWeight;
        this.parcelType = parcelType;
        this.courierCompanyName = courierCompanyName;
    }

}
