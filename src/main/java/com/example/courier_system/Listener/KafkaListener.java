package com.example.courier_system.Listener;

import org.springframework.stereotype.Component;

@Component
public class KafkaListener {

    //    @KafkaListeners(topics = "placeOrder",groupId="groupId1")
    void listener(String data) {
        System.out.println("Received Message in courier: " + data);
    }

    //    @KafkaListeners(topics = "updateOrder",groupId="groupId2")
    /*void listener(String data) {
        System.out.println("Received Message in courier: " + data);
    }*/

    //    @KafkaListeners(topics = "deleteOrder",groupId="groupId3")
    /*void listener(String data) {
        System.out.println("Received Message in courier: " + data);
    }*/
}
