package com.example.courier_system.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Bean
    public NewTopic courierServiceTopic() {
        return TopicBuilder.name("placeOrder").build();
    }

    @Bean
    public NewTopic courierServiceUpdateTopic() {
        return TopicBuilder.name("updateOrder").build();
    }

    @Bean
    public NewTopic courierServiceDeleteTopic() {
        return TopicBuilder.name("deleteOrder").build();
    }

}
