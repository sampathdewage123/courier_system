package com.example.courier_system.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "order_Details")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "sender_name")
    private String senderName;
    @Column(name = "sender_address")
    private String senderAddress;
    @Column(name = "receiver_name")
    private String receiverName;
    @Column(name = "receiver_address")
    private String receiverAddress;
    @Column(name = "weight")
    private String parcelWeight;
    @Column(name = "company_code")
    private Long courierCompanyCode;
    @Column(name = "type")
    private String parcelType;
    @Column(name = "user_id")
    private Long userId;

}
